import React, { Component } from "react";
import FooterAdmin from "../components/FooterAdmin";

import { Link } from "react-router-dom";

class Login extends Component {
  render() {
    return (
       
            <div>
                            <nav className="navbar navbar-default">
                <div className="container">
                    <div className="navbar-header">
                    <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span className="sr-only">Toggle navigation</span>
                        <span className="icon-bar" />
                        <span className="icon-bar" />
                        <span className="icon-bar" />
                    </button>
                    <a className="navbar-brand" href="#">Blog admin Dashboard</a>
                    </div>
                    <div id="navbar" className="collapse navbar-collapse">
                    <ul className="nav navbar-nav">
                        <li className="active"><a href="index.html">Dashboard</a></li>
                        <li><a href="pages.html">Pages</a></li>
                        <li><a href="posts.html">Posts</a></li>
                        <li><a href="users.html">Users</a></li>
                    </ul>
                    <ul className="nav navbar-nav navbar-right">
                        <li><a href="#">Welcome, Brad</a></li>
                        <li><a href="login.html">Logout</a></li>
                    </ul>
                    </div>
                </div>
                </nav>



  <header id="header">
    <div className="container">
      <div className="row">
        <div className="col-md-12">
          <h1 className="text-center"> Admin Area <small>Account Login</small></h1>
        </div>
      </div>
    </div>
  </header>
  <section id="main">
    <div className="container">
      <div className="row">
        <div className="col-md-4 col-md-offset-4">
          <form id="login" action="index.html" className="well">
            <div className="form-group">
              <label>Email Address</label>
              <input type="text" className="form-control" placeholder="Enter Email" />
            </div>
            <div className="form-group">
              <label>Password</label>
              <input type="password" className="form-control" placeholder="Password" />
            </div>
            <Link to="/admin/index"><button type="submit" className="btn btn-default btn-block">Login</button></Link>
          </form>
        </div>
      </div>
    </div>
  </section>
  <FooterAdmin/>
</div>


        );
    }
}

export default Login;
