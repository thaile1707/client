import React, { Component } from "react";
import FooterAdmin from "../components/FooterAdmin";
import NavbarAdmin from "../components/NavbarAdmin";
import '../libs/css/style.css';
import {Link} from "react-router-dom";


class MgUser extends Component {
  render() {
        var products = [];

    return (
            <div>
                
        <NavbarAdmin/>
  <header id="header">
    <div className="container">
      <div className="row">
        <div className="col-md-10">
          <h1><span className="glyphicon glyphicon-cog" aria-hidden="true" /> Users<small>Manage Site Users</small></h1>
        </div>
        <div className="col-md-2">
          <div className="dropdown create">
            <button className="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
              Create Content
              <span className="caret" />
            </button>
            <ul className="dropdown-menu" aria-labelledby="dropdownMenu1">
              <li><a type="button" data-toggle="modal" data-target="#addPage">Add Page</a></li>
              <li><a href="#">Add Post</a></li>
              <li><a href="#">Add User</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </header>
  <section id="breadcrumb">
    <div className="container">
      <ol className="breadcrumb">
        <li><a href="index.html">Dashboard</a></li>
        <li className="active">Users</li>
      </ol>
    </div>
  </section>
  <section id="main">
    <div className="container">
      <div className="row">
        <div className="col-md-3">
          <div className="list-group">
            <a href="index.html" className="list-group-item active main-color-bg">
              <span className="glyphicon glyphicon-cog" aria-hidden="true" /> Dashboard
            </a>
            <a href="pages.html" className="list-group-item"><span className="glyphicon glyphicon-list-alt" aria-hidden="true" /> Pages <span className="badge">12</span></a>
            <a href="posts.html" className="list-group-item"><span className="glyphicon glyphicon-pencil" aria-hidden="true" /> Posts <span className="badge">33</span></a>
            <Link to="/admin/editpost" className="list-group-item"><span className="glyphicon glyphicon-user" aria-hidden="true" /> Users <span className="badge">203</span></Link>
          </div>
          <div className="well">
            <h4>Disk Space Used</h4>
            <div className="progress">
              <div className="progress-bar" role="progressbar" aria-valuenow={60} aria-valuemin={0} aria-valuemax={100} style={{width: '60%'}}>
                60%
              </div>
            </div>
            <h4>Bandwidth Used </h4>
            <div className="progress">
              <div className="progress-bar" role="progressbar" aria-valuenow={40} aria-valuemin={0} aria-valuemax={100} style={{width: '40%'}}>
                40%
              </div>
            </div>
          </div>
        </div>
        <div className="col-md-9">
          {/* Website Overview */}
          <div className="panel panel-default">
            <div className="panel-heading main-color-bg">
              <h3 className="panel-title">Users</h3>
            </div>
            <div className="panel-body">
              <div className="row">
                <div className="col-md-12">
                  <input className="form-control" type="text" placeholder="Filter Users..." />
                </div>
              </div>
              <br />
              <table className="table table-striped table-hover">
                <tbody><tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Joined</th>
                    <th />
                  </tr>
                  <tr>
                    <td>Jill Smith</td>
                    <td>jillsmith@gmail.com</td>
                    <td>Dec 12, 2016</td>
                    <td><a className="btn btn-default" href="edit.html">Edit</a> <a className="btn btn-danger" href="#">Delete</a></td>
                  </tr>
                  <tr>
                    <td>Eve Jackson</td>
                    <td>ejackson@yahoo.com</td>
                    <td>Dec 13, 2016</td>
                    <td><a className="btn btn-default" href="edit.html">Edit</a> <a className="btn btn-danger" href="#">Delete</a></td>
                  </tr>
                  <tr>
                    <td>Stephanie Landon</td>
                    <td>landon@yahoo.com</td>
                    <td>Dec 14, 2016</td>
                    <td><a className="btn btn-default" href="edit.html">Edit</a> <a className="btn btn-danger" href="#">Delete</a></td>
                  </tr>
                  <tr>
                    <td>Mike Johnson</td>
                    <td>mjohnson@gmail.com</td>
                    <td>Dec 15, 2016</td>
                    <td><a className="btn btn-default" href="edit.html">Edit</a> <a className="btn btn-danger" href="#">Delete</a></td>
                  </tr>
                </tbody></table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  {/* Modals */}
  {/* Add Page */}
  <div className="modal fade" id="addPage" tabIndex={-1} role="dialog" aria-labelledby="myModalLabel">
    <div className="modal-dialog" role="document">
      <div className="modal-content">
        <form>
          <div className="modal-header">
            <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <h4 className="modal-title" id="myModalLabel">Add Page</h4>
          </div>
          <div className="modal-body">
            <div className="form-group">
              <label>Page Title</label>
              <input type="text" className="form-control" placeholder="Page Title" />
            </div>
            <div className="form-group">
              <label>Page Body</label>
              <textarea name="editor1" className="form-control" placeholder="Page Body" defaultValue={""} />
            </div>
            <div className="checkbox">
              <label>
                <input type="checkbox" /> Published
              </label>
            </div>
            <div className="form-group">
              <label>Meta Tags</label>
              <input type="text" className="form-control" placeholder="Add Some Tags..." />
            </div>
            <div className="form-group">
              <label>Meta Description</label>
              <input type="text" className="form-control" placeholder="Add Meta Description..." />
            </div>
          </div>
          <div className="modal-footer">
            <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" className="btn btn-primary">Save changes</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <FooterAdmin/>
</div>

    );
    }
}

export default MgUser;