import React, { Component } from "react";
import FooterAdmin from "../components/FooterAdmin";
import NavbarAdmin from "../components/NavbarAdmin";
import '../libs/css/style.css';



class EditPost extends Component {
  render() {
    return (
            <div>
                <NavbarAdmin/>
  <header id="header">
    <div className="container">
      <div className="row">
        <div className="col-md-10">
          <h1><span className="glyphicon glyphicon-cog" aria-hidden="true" /> Edit Page<small>About</small></h1>
        </div>
        <div className="col-md-2">
          <div className="dropdown create">
            <button className="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
              Create Content
              <span className="caret" />
            </button>
            <ul className="dropdown-menu" aria-labelledby="dropdownMenu1">
              <li><a type="button" data-toggle="modal" data-target="#addPage">Add Page</a></li>
              <li><a href="#">Add Post</a></li>
              <li><a href="#">Add User</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </header>
  <section id="breadcrumb">
    <div className="container">
      <ol className="breadcrumb">
        <li><a href="index.html">Dashboard</a></li>
        <li><a href="pages.html">Pages</a></li>
        <li className="active">Edit Page</li>
      </ol>
    </div>
  </section>
  <section id="main">
    <div className="container">
      <div className="row">
        <div className="col-md-3">
          <div className="list-group">
            <a href="index.html" className="list-group-item active main-color-bg">
              <span className="glyphicon glyphicon-cog" aria-hidden="true" /> Dashboard
            </a>
            <a href="pages.html" className="list-group-item"><span className="glyphicon glyphicon-list-alt" aria-hidden="true" /> Pages <span className="badge">12</span></a>
            <a href="posts.html" className="list-group-item"><span className="glyphicon glyphicon-pencil" aria-hidden="true" /> Posts <span className="badge">33</span></a>
            <a href="users.html" className="list-group-item"><span className="glyphicon glyphicon-user" aria-hidden="true" /> Users <span className="badge">203</span></a>
          </div>
          <div className="well">
            <h4>Disk Space Used</h4>
            <div className="progress">
              <div className="progress-bar" role="progressbar" aria-valuenow={60} aria-valuemin={0} aria-valuemax={100} style={{width: '60%'}}>
                60%
              </div>
            </div>
            <h4>Bandwidth Used </h4>
            <div className="progress">
              <div className="progress-bar" role="progressbar" aria-valuenow={40} aria-valuemin={0} aria-valuemax={100} style={{width: '40%'}}>
                40%
              </div>
            </div>
          </div>
        </div>
        <div className="col-md-9">
          {/* Website Overview */}
          <div className="panel panel-default">
            <div className="panel-heading main-color-bg">
              <h3 className="panel-title">Edit Page</h3>
            </div>
            <div className="panel-body">
              <form>
                <div className="form-group">
                  <label>Page Title</label>
                  <input type="text" className="form-control" placeholder="Page Title" defaultValue="About" />
                </div>
                <div className="form-group">
                  <label>Page Body</label>
                  <textarea name="editor1" className="form-control" placeholder="Page Body" defaultValue={"                      Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\n                    "} />
                </div>
                <div className="checkbox">
                  <label>
                    <input type="checkbox" defaultChecked /> Published
                  </label>
                </div>
                <div className="form-group">
                  <label>Meta Tags</label>
                  <input type="text" className="form-control" placeholder="Add Some Tags..." defaultValue="tag1, tag2" />
                </div>
                <div className="form-group">
                  <label>Meta Description</label>
                  <input type="text" className="form-control" placeholder="Add Meta Description..." defaultValue="  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et " />
                </div>
                <input type="submit" className="btn btn-default" defaultValue="Submit" />
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
 
  {/* Modals */}
  {/* Add Page */}
  <div className="modal fade" id="addPage" tabIndex={-1} role="dialog" aria-labelledby="myModalLabel">
    <div className="modal-dialog" role="document">
      <div className="modal-content">
        <form>
          <div className="modal-header">
            <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <h4 className="modal-title" id="myModalLabel">Add Page</h4>
          </div>
          <div className="modal-body">
            <div className="form-group">
              <label>Page Title</label>
              <input type="text" className="form-control" placeholder="Page Title" />
            </div>
            <div className="form-group">
              <label>Page Body</label>
              <textarea name="editor1" className="form-control" placeholder="Page Body" defaultValue={""} />
            </div>
            <div className="checkbox">
              <label>
                <input type="checkbox" /> Published
              </label>
            </div>
            <div className="form-group">
              <label>Meta Tags</label>
              <input type="text" className="form-control" placeholder="Add Some Tags..." />
            </div>
            <div className="form-group">
              <label>Meta Description</label>
              <input type="text" className="form-control" placeholder="Add Meta Description..." />
            </div>
          </div>
          <div className="modal-footer">
            <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" className="btn btn-primary">Save changes</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <FooterAdmin/>
</div>

    );
    }
}

export default EditPost;