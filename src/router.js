import React from "react";
import { Route, Switch } from "react-router-dom";
import Home from "./web/Home";
import About from "./web/About";
import Signin from "./web/Login";
import Category from "./web/Category";
import Post from "./web/Post";
import Login from "./admin/Login";
import Index from "./admin/Index";
import MgPost from "./admin/Post";
import EditPost from "./admin/EditPost";
import MgUser from "./admin/User";




const router = (
    <Switch>
  
        <Route exact path="/" render={props => <Home {...props} />} />} />
        <Route  path="/about" render={props => <About {...props} />} />
        <Route  path="/login" render={props => <Signin {...props} />} />
        <Route  path="/cate" render={props => <Category {...props} />} />
         <Route  path="/post" render={props => <Post {...props} />} />
         <Route  path="/admin/login" render={props => <Login {...props} />} />
         <Route  path="/admin/index" render={props => <Index {...props} />} />
         <Route  path="/admin/post" render={props => <MgPost {...props} />} />
         <Route  path="/admin/editpost" render={props => <EditPost {...props} />} />
         <Route  path="/admin/user" render={props => <MgUser {...props} />} />
    
    
    

    </Switch>
  
 
  );

export default router;
