import React from "react";
import "./App.css";
import { BrowserRouter as Router } from "react-router-dom";
import routes from "./router";


function App() {
  return (
    <Router>
      <React.Fragment>

        {routes}
      </React.Fragment>
    </Router>
  );
}

export default App;
