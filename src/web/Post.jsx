import React, { Component } from "react";
import Footer from "../components/Footer";
import Navbar from "../components/Navbar";
import '../libs/styles/post.css'
import '../libs/styles/post_responsive.css'


class Post extends Component {
  render() {
    return (
        <React.Fragment>
            <Navbar/>
                      <div className="home">
            <div className="home_background parallax-window" data-parallax="scroll" data-image-src={process.env.PUBLIC_URL + "/assets/regular.jpg"} data-speed="0.8" />
        </div>
                    <div className="page_content">
                      <div className="container">
                        <div className="row row-lg-eq-height">
                          {/* Post Content */}
                          <div className="col-lg-9">
                            <div className="post_content">
                              {/* Top Panel */}
                              <div className="post_panel post_panel_top d-flex flex-row align-items-center justify-content-start">
                                <div className="author_image"><div><img src={process.env.PUBLIC_URL + "/assets/author.jpg" }alt /></div></div>
                                <div className="post_meta"><a href="#">Katy Liu</a><span>Sep 29, 2017 at 9:48 am</span></div>
                                <div className="post_share ml-sm-auto">
                                  <span>share</span>
                                  <ul className="post_share_list">
                                    <li className="post_share_item"><a href="#"><i className="fa fa-facebook" aria-hidden="true" /></a></li>
                                    <li className="post_share_item"><a href="#"><i className="fa fa-twitter" aria-hidden="true" /></a></li>
                                    <li className="post_share_item"><a href="#"><i className="fa fa-google" aria-hidden="true" /></a></li>
                                  </ul>
                                </div>
                              </div>
                              {/* Post Body */}
                              <div className="post_body">
                                <p className="post_p">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce enim nulla, mollis eu metus in, sagittis fringilla tortor. Phasellus eget purus id felis dignissim convallis. Suspendisse et augue dui. Morbi gravida sed justo vel venenatis. Ut tempor pretium maximus. Donec libero diam, faucibus vitae lectus nec, accumsan gravida dui. Nam interdum mi eget lacus aliquet, sit amet ultricies magna pharetra. In ut odio a ligula egestas pretium et quis sapien. Etiam faucibus magna eu porta vulputate. Aliquam euismod rhoncus malesuada. Nunc rutrum hendrerit semper.</p>
                                <figure>
                                  <img src={process.env.PUBLIC_URL + "/assets/post_image.jpg" }alt />
                                  <figcaption>Lorem Ipsum Dolor Sit Amet</figcaption>
                                </figure>
                                <p className="post_p">Maecenas vitae sem varius, imperdiet nisi a, tristique nisi. Sed scelerisque suscipit leo cursus accumsan. Donec vel turpis quam. Mauris non nisl nec nunc gravida ullamcorper id vestibulum magna. Donec non velit finibus, laoreet arcu nec, facilisis augue. Aliquam sed purus id erat accumsan congue. Mauris semper ullamcorper nibh non pellentesque. Aenean euismod purus sit amet quam vehicula ornare.</p>
                                <div className="post_quote">
                                  <p className="post_p">Aliquam auctor lacus a dapibus pulvinar. Morbi in elit erat. Quisque et augue nec tortor blandit hendrerit eget sit amet sapien. Curabitur at tincidunt metus, quis porta ex. Duis lacinia metus vel eros cursus pretium eget.</p>
                                  <div className="post_quote_source">Robert Morgan</div>
                                </div>
                                <p className="post_p">Donec orci dolor, pretium in luctus id, consequat vitae nibh. Quisque hendrerit, lorem sit amet mollis malesuada, urna orci volutpat ex, sed scelerisque nunc velit et massa. Sed maximus id erat vel feugiat. Phasellus bibendum nisi non urna bibendum elementum. Aenean tincidunt nibh vitae ex facilisis ultrices. Integer ornare efficitur ultrices. Integer neque lectus, venenatis at pulvinar quis, aliquet id neque. Mauris ultrices consequat velit, sed dignissim elit posuere in. Cras vitae dictum dui.</p>
                                {/* Post Tags */}
                                <div className="post_tags">
                                  <ul>
                                    <li className="post_tag"><a href="#">Liberty</a></li>
                                    <li className="post_tag"><a href="#">Manual</a></li>
                                    <li className="post_tag"><a href="#">Interpretation</a></li>
                                    <li className="post_tag"><a href="#">Recommendation</a></li>
                                  </ul>
                                </div>
                              </div>
                              {/* Bottom Panel */}
                              <div className="post_panel bottom_panel d-flex flex-row align-items-center justify-content-start">
                                <div className="author_image"><div><img src={process.env.PUBLIC_URL + "/assets/author.jpg" }alt /></div></div>
                                <div className="post_meta"><a href="#">Katy Liu</a><span>Sep 29, 2017 at 9:48 am</span></div>
                                <div className="post_share ml-sm-auto">
                                  <span>share</span>
                                  <ul className="post_share_list">
                                    <li className="post_share_item"><a href="#"><i className="fa fa-facebook" aria-hidden="true" /></a></li>
                                    <li className="post_share_item"><a href="#"><i className="fa fa-twitter" aria-hidden="true" /></a></li>
                                    <li className="post_share_item"><a href="#"><i className="fa fa-google" aria-hidden="true" /></a></li>
                                  </ul>
                                </div>
                              </div>
                              {/* Similar Posts */}
                              <div className="similar_posts">
                            
                                {/* Post Comment */}
                                <div className="post_comment">
                                  <div className="post_comment_title">Post Comment</div>
                                  <div className="row">
                                    <div className="col-xl-8">
                                      <div className="post_comment_form_container">
                                        <form action="#">
                                          <input type="text" className="comment_input comment_input_name" placeholder="Your Name" required="required" />
                                          <input type="email" className="comment_input comment_input_email" placeholder="Your Email" required="required" />
                                          <textarea className="comment_text" placeholder="Your Comment" required="required" defaultValue={""} />
                                          <button type="submit" className="comment_button">Post Comment</button>
                                        </form>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                {/* Comments */}
                                <div className="comments">
                                  <div className="comments_title">Comments <span>(12)</span></div>
                                  <div className="row">
                                    <div className="col-xl-8">
                                      <div className="comments_container">
                                        <ul className="comment_list">
                                          <li className="comment">
                                            <div className="comment_body">
                                              <div className="comment_panel d-flex flex-row align-items-center justify-content-start">
                                                <div className="comment_author_image"><div><img src={process.env.PUBLIC_URL + "/assets/comment_author_1.jpg" }alt /></div></div>
                                                <small className="post_meta"><a href="#">Katy Liu</a><span>Sep 29, 2017 at 9:48 am</span></small>
                                                <button type="button" className="reply_button ml-auto">Reply</button>
                                              </div>
                                              <div className="comment_content">
                                                <p>Pick the yellow peach that looks like a sunset with its red, orange, and pink coat skin, peel it off with your teeth. Sink them into unripened.</p>
                                              </div>
                                            </div>
                                            {/* Reply */}
                                            <ul className="comment_list">
                                              <li className="comment">
                                                <div className="comment_body">
                                                  <div className="comment_panel d-flex flex-row align-items-center justify-content-start">
                                                    <div className="comment_author_image"><div><img src={process.env.PUBLIC_URL + "/assets/comment_author_2.jpg" }alt /></div></div>
                                                    <small className="post_meta"><a href="#">Katy Liu</a><span>Sep 29, 2017 at 9:48 am</span></small>
                                                    <button type="button" className="reply_button ml-auto">Reply</button>
                                                  </div>
                                                  <div className="comment_content">
                                                    <p>Nulla facilisi. Aenean porttitor quis tortor id tempus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vivamus molestie varius tincidunt. Vestibulum congue sed libero ac sodales.</p>
                                                  </div>
                                                </div>
                                                {/* Reply */}
                                                <ul className="comment_list">
                                                </ul>
                                              </li>
                                            </ul>
                                          </li>
                                          <li className="comment">
                                            <div className="comment_body">
                                              <div className="comment_panel d-flex flex-row align-items-center justify-content-start">
                                                <div className="comment_author_image"><div><img src={process.env.PUBLIC_URL + "/assets/comment_author_1.jpg" }alt /></div></div>
                                                <small className="post_meta"><a href="#">Katy Liu</a><span>Sep 29, 2017 at 9:48 am</span></small>
                                                <button type="button" className="reply_button ml-auto">Reply</button>
                                              </div>
                                              <div className="comment_content">
                                                <p>Pick the yellow peach that looks like a sunset with its red, orange, and pink coat skin, peel it off with your teeth. Sink them into unripened.</p>
                                              </div>
                                            </div>
                                          </li>
                                        </ul>
                                      </div>
                                    </div>
                                  </div>    
                                </div>
                              </div>
                            </div>
                            <div className="load_more">
                              <div id="load_more" className="load_more_button text-center trans_200">Load More</div>
                            </div>
                          </div>
                          {/* Sidebar */}
                          <div className="col-lg-3">
                            <div className="sidebar">
                              <div className="sidebar_background" />
                              {/* Top Stories */}
                              <div className="sidebar_section">
                                <div className="sidebar_title_container">
                                  <div className="sidebar_title">Top Stories</div>
                                  <div className="sidebar_slider_nav">
                                    <div className="custom_nav_container sidebar_slider_nav_container">
                                      <div className="custom_prev custom_prev_top">
                                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="7px" height="12px" viewBox="0 0 7 12" enableBackground="new 0 0 7 12" xmlSpace="preserve">
                                          <polyline fill="#bebebe" points="0,5.61 5.609,0 7,0 7,1.438 2.438,6 7,10.563 7,12 5.609,12 -0.002,6.39 " />
                                        </svg>
                                      </div>
                                      <ul id="custom_dots" className="custom_dots custom_dots_top">
                                        <li className="custom_dot custom_dot_top active"><span /></li>
                                        <li className="custom_dot custom_dot_top"><span /></li>
                                        <li className="custom_dot custom_dot_top"><span /></li>
                                      </ul>
                                      <div className="custom_next custom_next_top">
                                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="7px" height="12px" viewBox="0 0 7 12" enableBackground="new 0 0 7 12" xmlSpace="preserve">
                                          <polyline fill="#bebebe" points="6.998,6.39 1.389,12 -0.002,12 -0.002,10.562 4.561,6 -0.002,1.438 -0.002,0 1.389,0 7,5.61 " />
                                        </svg>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div className="sidebar_section_content">
                                  {/* Top Stories Slider */}
                                  <div className="sidebar_slider_container">
                                    <div className="owl-carousel owl-theme sidebar_slider_top">
                                      {/* Top Stories Slider Item */}
                                      <div className="owl-item">
                                        {/* Sidebar Post */}
                                        <div className="side_post">
                                          <a href="post.html">
                                            <div className="d-flex flex-row align-items-xl-center align-items-start justify-content-start">
                                              <div className="side_post_image"><div><img src={process.env.PUBLIC_URL + "/assets/top_1.jpg" }alt /></div></div>
                                              <div className="side_post_content">
                                                <div className="side_post_title">How Did van Gogh’s Turbulent Mind</div>
                                                <small className="post_meta">Katy Liu<span>Sep 29</span></small>
                                              </div>
                                            </div>
                                          </a>
                                        </div>
                                        {/* Sidebar Post */}
                                        <div className="side_post">
                                          <a href="post.html">
                                            <div className="d-flex flex-row align-items-xl-center align-items-start justify-content-start">
                                              <div className="side_post_image"><div><img src={process.env.PUBLIC_URL + "/assets/top_2.jpg" }alt /></div></div>
                                              <div className="side_post_content">
                                                <div className="side_post_title">How Did van Gogh’s Turbulent Mind</div>
                                                <small className="post_meta">Katy Liu<span>Sep 29</span></small>
                                              </div>
                                            </div>
                                          </a>
                                        </div>
                                        {/* Sidebar Post */}
                                        <div className="side_post">
                                          <a href="post.html">
                                            <div className="d-flex flex-row align-items-xl-center align-items-start justify-content-start">
                                              <div className="side_post_image"><div><img src={process.env.PUBLIC_URL + "/assets/top_3.jpg" }alt /></div></div>
                                              <div className="side_post_content">
                                                <div className="side_post_title">How Did van Gogh’s Turbulent Mind</div>
                                                <small className="post_meta">Katy Liu<span>Sep 29</span></small>
                                              </div>
                                            </div>
                                          </a>
                                        </div>
                                        {/* Sidebar Post */}
                                        <div className="side_post">
                                          <a href="post.html">
                                            <div className="d-flex flex-row align-items-xl-center align-items-start justify-content-start">
                                              <div className="side_post_image"><div><img src={process.env.PUBLIC_URL + "/assets/top_4.jpg" }alt /></div></div>
                                              <div className="side_post_content">
                                                <div className="side_post_title">How Did van Gogh’s Turbulent Mind</div>
                                                <small className="post_meta">Katy Liu<span>Sep 29</span></small>
                                              </div>
                                            </div>
                                          </a>
                                        </div>
                                      </div>
                                      {/* Top Stories Slider Item */}
                                      <div className="owl-item">
                                        {/* Sidebar Post */}
                                        <div className="side_post">
                                          <a href="post.html">
                                            <div className="d-flex flex-row align-items-xl-center align-items-start justify-content-start">
                                              <div className="side_post_image"><div><img src={process.env.PUBLIC_URL + "/assets/top_1.jpg" }alt /></div></div>
                                              <div className="side_post_content">
                                                <div className="side_post_title">How Did van Gogh’s Turbulent Mind</div>
                                                <small className="post_meta">Katy Liu<span>Sep 29</span></small>
                                              </div>
                                            </div>
                                          </a>
                                        </div>
                                        {/* Sidebar Post */}
                                        <div className="side_post">
                                          <a href="post.html">
                                            <div className="d-flex flex-row align-items-xl-center align-items-start justify-content-start">
                                              <div className="side_post_image"><div><img src={process.env.PUBLIC_URL + "/assets/top_2.jpg" }alt /></div></div>
                                              <div className="side_post_content">
                                                <div className="side_post_title">How Did van Gogh’s Turbulent Mind</div>
                                                <small className="post_meta">Katy Liu<span>Sep 29</span></small>
                                              </div>
                                            </div>
                                          </a>
                                        </div>
                                        {/* Sidebar Post */}
                                        <div className="side_post">
                                          <a href="post.html">
                                            <div className="d-flex flex-row align-items-xl-center align-items-start justify-content-start">
                                              <div className="side_post_image"><div><img src={process.env.PUBLIC_URL + "/assets/top_3.jpg" }alt /></div></div>
                                              <div className="side_post_content">
                                                <div className="side_post_title">How Did van Gogh’s Turbulent Mind</div>
                                                <small className="post_meta">Katy Liu<span>Sep 29</span></small>
                                              </div>
                                            </div>
                                          </a>
                                        </div>
                                        {/* Sidebar Post */}
                                        <div className="side_post">
                                          <a href="post.html">
                                            <div className="d-flex flex-row align-items-xl-center align-items-start justify-content-start">
                                              <div className="side_post_image"><div><img src={process.env.PUBLIC_URL + "/assets/top_4.jpg" }alt /></div></div>
                                              <div className="side_post_content">
                                                <div className="side_post_title">How Did van Gogh’s Turbulent Mind</div>
                                                <small className="post_meta">Katy Liu<span>Sep 29</span></small>
                                              </div>
                                            </div>
                                          </a>
                                        </div>
                                      </div>
                                      {/* Top Stories Slider Item */}
                                      <div className="owl-item">
                                        {/* Sidebar Post */}
                                        <div className="side_post">
                                          <a href="post.html">
                                            <div className="d-flex flex-row align-items-xl-center align-items-start justify-content-start">
                                              <div className="side_post_image"><div><img src={process.env.PUBLIC_URL + "/assets/top_1.jpg" }alt /></div></div>
                                              <div className="side_post_content">
                                                <div className="side_post_title">How Did van Gogh’s Turbulent Mind</div>
                                                <small className="post_meta">Katy Liu<span>Sep 29</span></small>
                                              </div>
                                            </div>
                                          </a>
                                        </div>
                                        {/* Sidebar Post */}
                                        <div className="side_post">
                                          <a href="post.html">
                                            <div className="d-flex flex-row align-items-xl-center align-items-start justify-content-start">
                                              <div className="side_post_image"><div><img src={process.env.PUBLIC_URL + "/assets/top_2.jpg" }alt /></div></div>
                                              <div className="side_post_content">
                                                <div className="side_post_title">How Did van Gogh’s Turbulent Mind</div>
                                                <small className="post_meta">Katy Liu<span>Sep 29</span></small>
                                              </div>
                                            </div>
                                          </a>
                                        </div>
                                        {/* Sidebar Post */}
                                        <div className="side_post">
                                          <a href="post.html">
                                            <div className="d-flex flex-row align-items-xl-center align-items-start justify-content-start">
                                              <div className="side_post_image"><div><img src={process.env.PUBLIC_URL + "/assets/top_3.jpg" }alt /></div></div>
                                              <div className="side_post_content">
                                                <div className="side_post_title">How Did van Gogh’s Turbulent Mind</div>
                                                <small className="post_meta">Katy Liu<span>Sep 29</span></small>
                                              </div>
                                            </div>
                                          </a>
                                        </div>
                                        {/* Sidebar Post */}
                                        <div className="side_post">
                                          <a href="post.html">
                                            <div className="d-flex flex-row align-items-xl-center align-items-start justify-content-start">
                                              <div className="side_post_image"><div><img src={process.env.PUBLIC_URL + "/assets/top_4.jpg" }alt /></div></div>
                                              <div className="side_post_content">
                                                <div className="side_post_title">How Did van Gogh’s Turbulent Mind</div>
                                                <small className="post_meta">Katy Liu<span>Sep 29</span></small>
                                              </div>
                                            </div>
                                          </a>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            
                            
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

            <Footer/>
        </React.Fragment>
           


        );
    }
}

export default Post;
