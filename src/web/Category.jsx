import React, { Component } from "react";
import Navbar from "../components/Navbar";
import Footer from "../components/Footer";
import '../libs/styles/category.css'
import '../libs/styles/category_responsive.css';
import { Link } from "react-router-dom";

class Category extends Component {
  render() {
    return (
      <div>

        <Navbar/>
        {/* Home */}
        <div className="home">
            <div className="home_background parallax-window" data-parallax="scroll" data-image-src={process.env.PUBLIC_URL + "/assets/regular.jpg"} data-speed="0.8" />
        </div>

        {/* Page Content */}
        <div className="page_content">
          <div className="container">
            <div className="row row-lg-eq-height">
              {/* Main Content */}
              <div className="col-lg-9">
                <div className="main_content">
                  {/* Category */}
                  <div className="category">
                    <div className="section_panel d-flex flex-row align-items-center justify-content-start">
                      <div className="section_title">Don't Miss</div>
                      <div className="section_tags ml-auto">
                        <ul>
                          <li className="active"><a href="category.html">all</a></li>
                          <li><a href="category.html">style hunter</a></li>
                          <li><a href="category.html">vogue</a></li>
                          <li><a href="category.html">health &amp; fitness</a></li>
                          <li><a href="category.html">travel</a></li>
                        </ul>
                      </div>
                      <div className="section_panel_more">
                        <ul>
                          <li>more
                            <ul>
                              <li><a href="category.html">new look 2018</a></li>
                              <li><a href="category.html">street fashion</a></li>
                              <li><a href="category.html">business</a></li>
                              <li><a href="category.html">recipes</a></li>
                              <li><a href="category.html">sport</a></li>
                              <li><a href="category.html">celebrities</a></li>
                            </ul>
                          </li>
                        </ul>
                      </div>
                    </div>
                    <div className="section_content">
                      <div className="grid clearfix">
                        {/* Small Card With Image */}
                        <div className="card card_small_with_image grid-item">
                          <img className="card-img-top" src={process.env.PUBLIC_URL + "/assets/post_10.jpg"} alt />
                          <div className="card-body">
                            <div className="card-title card-title-small"><Link to="/post">How Did van Gogh’s Turbulent Mind Depict One of the Most Complex Concepts in Physics?</Link></div>
                            <small className="post_meta"><a href="#">Katy Liu</a><span>Sep 29, 2017 at 9:48 am</span></small>
                          </div>
                        </div>
                        {/* Small Card Without Image */}
                        {/* Small Card With Image */}
                        <div className="card card_small_with_image grid-item">
                          <img className="card-img-top" src={process.env.PUBLIC_URL + "/assets/post_15.jpg"} alt />
                          <div className="card-body">
                            <div className="card-title card-title-small"><a href="post.html">How Did van Gogh’s Turbulent Mind Depict One of the Most Complex Concepts in Physics?</a></div>
                            <small className="post_meta"><a href="#">Katy Liu</a><span>Sep 29, 2017 at 9:48 am</span></small>
                          </div>
                        </div>
                        {/* Small Card With Image */}
                        <div className="card card_small_with_image grid-item">
                          <img className="card-img-top" src={process.env.PUBLIC_URL + "/assets/post_13.jpg"} alt="https://unsplash.com/@jakobowens1" />
                          <div className="card-body">
                            <div className="card-title card-title-small"><a href="post.html">How Did van Gogh’s Turbulent Mind Depict One of the Most Complex Concepts in Physics?</a></div>
                            <small className="post_meta"><a href="#">Katy Liu</a><span>Sep 29, 2017 at 9:48 am</span></small>
                          </div>
                        </div>
                        {/* Small Card With Image */}
                        <div className="card card_small_with_image grid-item">
                          <img className="card-img-top" src={process.env.PUBLIC_URL + "/assets/post_14.jpg"} alt />
                          <div className="card-body">
                            <div className="card-title card-title-small"><a href="post.html">How Did van Gogh’s Turbulent Mind Depict One of the Most Complex Concepts in Physics?</a></div>
                            <small className="post_meta"><a href="#">Katy Liu</a><span>Sep 29, 2017 at 9:48 am</span></small>
                          </div>
                        </div>
                       
                        <div className="card card_small_with_image grid-item">
                          <img className="card-img-top" src={process.env.PUBLIC_URL + "/assets/post_5.jpg"} alt />
                          <div className="card-body">
                            <div className="card-title card-title-small"><a href="post.html">How Did van Gogh’s Turbulent Mind Depict One of the Most Complex Concepts in Physics?</a></div>
                            <small className="post_meta"><a href="#">Katy Liu</a><span>Sep 29, 2017 at 9:48 am</span></small>
                          </div>
                        </div>
                        {/* Small Card With Image */}
                        <div className="card card_small_with_image grid-item">
                          <img className="card-img-top" src={process.env.PUBLIC_URL + "/assets/post_10.jpg"} alt />
                          <div className="card-body">
                            <div className="card-title card-title-small"><a href="post.html">How Did van Gogh’s Turbulent Mind Depict One of the Most Complex Concepts in Physics?</a></div>
                            <small className="post_meta"><a href="#">Katy Liu</a><span>Sep 29, 2017 at 9:48 am</span></small>
                          </div>
                        </div>
                        {/* Small Card Without Image */}
                        {/* Small Card With Image */}
                        <div className="card card_small_with_image grid-item">
                          <img className="card-img-top" src={process.env.PUBLIC_URL + "/assets/post_20.jpg"} alt="https://unsplash.com/@jakobowens1" />
                          <div className="card-body">
                            <div className="card-title card-title-small"><a href="post.html">How Did van Gogh’s Turbulent Mind Depict One of the Most Complex Concepts in Physics?</a></div>
                            <small className="post_meta"><a href="#">Katy Liu</a><span>Sep 29, 2017 at 9:48 am</span></small>
                          </div>
                        </div>
                        
                        {/* Small Card With Image */}
                        <div className="card card_small_with_image grid-item">
                          <img className="card-img-top" src={process.env.PUBLIC_URL + "/assets/post_22.jpg"} alt />
                          <div className="card-body">
                            <div className="card-title card-title-small"><a href="post.html">How Did van Gogh’s Turbulent Mind Depict One of the Most Complex Concepts in Physics?</a></div>
                            <small className="post_meta"><a href="#">Katy Liu</a><span>Sep 29, 2017 at 9:48 am</span></small>
                          </div>
                        </div>
                        {/* Small Card Without Image */}
                     
                        {/* Default Card With Background */}
                        <div className="card card_default card_default_with_background grid-item">
                          <div className="card_background" style={{backgroundImage: 'url(images/post_24.jpg)'}} />
                          <div className="card-body">
                            <div className="card-title card-title-small"><a href="post.html">How Did van Gogh’s Turbulent Mind Depict One of the Most</a></div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="load_more">
                  <div id="load_more" className="load_more_button text-center trans_200">Load More</div>
                </div>
              </div>
              {/* Sidebar */}
              <div className="col-lg-3">
                <div className="sidebar">
                  <div className="sidebar_background" />
                  {/* Top Stories */}
                  <div className="sidebar_section">
                    <div className="sidebar_title_container">
                      <div className="sidebar_title">Top Stories</div>
                      <div className="sidebar_slider_nav">
                        <div className="custom_nav_container sidebar_slider_nav_container">
                          <div className="custom_prev custom_prev_top">
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="7px" height="12px" viewBox="0 0 7 12" enableBackground="new 0 0 7 12" xmlSpace="preserve">
                              <polyline fill="#bebebe" points="0,5.61 5.609,0 7,0 7,1.438 2.438,6 7,10.563 7,12 5.609,12 -0.002,6.39 " />
                            </svg>
                          </div>
                          <ul id="custom_dots" className="custom_dots custom_dots_top">
                            <li className="custom_dot custom_dot_top active"><span /></li>
                            <li className="custom_dot custom_dot_top"><span /></li>
                            <li className="custom_dot custom_dot_top"><span /></li>
                          </ul>
                          <div className="custom_next custom_next_top">
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="7px" height="12px" viewBox="0 0 7 12" enableBackground="new 0 0 7 12" xmlSpace="preserve">
                              <polyline fill="#bebebe" points="6.998,6.39 1.389,12 -0.002,12 -0.002,10.562 4.561,6 -0.002,1.438 -0.002,0 1.389,0 7,5.61 " />
                            </svg>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="sidebar_section_content">
                      {/* Top Stories Slider */}
                      <div className="sidebar_slider_container">
                        <div className="owl-carousel owl-theme sidebar_slider_top">
                          {/* Top Stories Slider Item */}
                          <div className="owl-item">
                            {/* Sidebar Post */}
                            <div className="side_post">
                              <a href="post.html">
                                <div className="d-flex flex-row align-items-xl-center align-items-start justify-content-start">
                                  <div className="side_post_image"><div><img src={process.env.PUBLIC_URL + "/assets/top_1.jpg"} alt /></div></div>
                                  <div className="side_post_content">
                                    <div className="side_post_title">How Did van Gogh’s Turbulent Mind</div>
                                    <small className="post_meta">Katy Liu<span>Sep 29</span></small>
                                  </div>
                                </div>
                              </a>
                            </div>
                            {/* Sidebar Post */}
                            <div className="side_post">
                              <a href="post.html">
                                <div className="d-flex flex-row align-items-xl-center align-items-start justify-content-start">
                                  <div className="side_post_image"><div><img src={process.env.PUBLIC_URL + "/assets/top_2.jpg"} alt /></div></div>
                                  <div className="side_post_content">
                                    <div className="side_post_title">How Did van Gogh’s Turbulent Mind</div>
                                    <small className="post_meta">Katy Liu<span>Sep 29</span></small>
                                  </div>
                                </div>
                              </a>
                            </div>
                            {/* Sidebar Post */}
                            <div className="side_post">
                              <a href="post.html">
                                <div className="d-flex flex-row align-items-xl-center align-items-start justify-content-start">
                                  <div className="side_post_image"><div><img src={process.env.PUBLIC_URL + "/assets/top_3.jpg"} alt /></div></div>
                                  <div className="side_post_content">
                                    <div className="side_post_title">How Did van Gogh’s Turbulent Mind</div>
                                    <small className="post_meta">Katy Liu<span>Sep 29</span></small>
                                  </div>
                                </div>
                              </a>
                            </div>
                            {/* Sidebar Post */}
                            <div className="side_post">
                              <a href="post.html">
                                <div className="d-flex flex-row align-items-xl-center align-items-start justify-content-start">
                                  <div className="side_post_image"><div><img src={process.env.PUBLIC_URL + "/assets/top_4.jpg"} alt /></div></div>
                                  <div className="side_post_content">
                                    <div className="side_post_title">How Did van Gogh’s Turbulent Mind</div>
                                    <small className="post_meta">Katy Liu<span>Sep 29</span></small>
                                  </div>
                                </div>
                              </a>
                            </div>
                          </div>
                          {/* Top Stories Slider Item */}
                          <div className="owl-item">
                            {/* Sidebar Post */}
                            <div className="side_post">
                              <a href="post.html">
                                <div className="d-flex flex-row align-items-xl-center align-items-start justify-content-start">
                                  <div className="side_post_image"><div><img src={process.env.PUBLIC_URL + "/assets/top_1.jpg"} alt /></div></div>
                                  <div className="side_post_content">
                                    <div className="side_post_title">How Did van Gogh’s Turbulent Mind</div>
                                    <small className="post_meta">Katy Liu<span>Sep 29</span></small>
                                  </div>
                                </div>
                              </a>
                            </div>
                            {/* Sidebar Post */}
                            <div className="side_post">
                              <a href="post.html">
                                <div className="d-flex flex-row align-items-xl-center align-items-start justify-content-start">
                                  <div className="side_post_image"><div><img src={process.env.PUBLIC_URL + "/assets/top_2.jpg"} alt /></div></div>
                                  <div className="side_post_content">
                                    <div className="side_post_title">How Did van Gogh’s Turbulent Mind</div>
                                    <small className="post_meta">Katy Liu<span>Sep 29</span></small>
                                  </div>
                                </div>
                              </a>
                            </div>
                            {/* Sidebar Post */}
                            <div className="side_post">
                              <a href="post.html">
                                <div className="d-flex flex-row align-items-xl-center align-items-start justify-content-start">
                                  <div className="side_post_image"><div><img src={process.env.PUBLIC_URL + "/assets/top_3.jpg"} alt /></div></div>
                                  <div className="side_post_content">
                                    <div className="side_post_title">How Did van Gogh’s Turbulent Mind</div>
                                    <small className="post_meta">Katy Liu<span>Sep 29</span></small>
                                  </div>
                                </div>
                              </a>
                            </div>
                            {/* Sidebar Post */}
                            <div className="side_post">
                              <a href="post.html">
                                <div className="d-flex flex-row align-items-xl-center align-items-start justify-content-start">
                                  <div className="side_post_image"><div><img src={process.env.PUBLIC_URL + "/assets/top_4.jpg"} alt /></div></div>
                                  <div className="side_post_content">
                                    <div className="side_post_title">How Did van Gogh’s Turbulent Mind</div>
                                    <small className="post_meta">Katy Liu<span>Sep 29</span></small>
                                  </div>
                                </div>
                              </a>
                            </div>
                          </div>
                          {/* Top Stories Slider Item */}
                          <div className="owl-item">
                            {/* Sidebar Post */}
                            <div className="side_post">
                              <a href="post.html">
                                <div className="d-flex flex-row align-items-xl-center align-items-start justify-content-start">
                                  <div className="side_post_image"><div><img src={process.env.PUBLIC_URL + "/assets/top_1.jpg"} alt /></div></div>
                                  <div className="side_post_content">
                                    <div className="side_post_title">How Did van Gogh’s Turbulent Mind</div>
                                    <small className="post_meta">Katy Liu<span>Sep 29</span></small>
                                  </div>
                                </div>
                              </a>
                            </div>
                            {/* Sidebar Post */}
                            <div className="side_post">
                              <a href="post.html">
                                <div className="d-flex flex-row align-items-xl-center align-items-start justify-content-start">
                                  <div className="side_post_image"><div><img src={process.env.PUBLIC_URL + "/assets/top_2.jpg"} alt /></div></div>
                                  <div className="side_post_content">
                                    <div className="side_post_title">How Did van Gogh’s Turbulent Mind</div>
                                    <small className="post_meta">Katy Liu<span>Sep 29</span></small>
                                  </div>
                                </div>
                              </a>
                            </div>
                            {/* Sidebar Post */}
                            <div className="side_post">
                              <a href="post.html">
                                <div className="d-flex flex-row align-items-xl-center align-items-start justify-content-start">
                                  <div className="side_post_image"><div><img src={process.env.PUBLIC_URL + "/assets/top_3.jpg"} alt /></div></div>
                                  <div className="side_post_content">
                                    <div className="side_post_title">How Did van Gogh’s Turbulent Mind</div>
                                    <small className="post_meta">Katy Liu<span>Sep 29</span></small>
                                  </div>
                                </div>
                              </a>
                            </div>
                            {/* Sidebar Post */}
                            <div className="side_post">
                              <a href="post.html">
                                <div className="d-flex flex-row align-items-xl-center align-items-start justify-content-start">
                                  <div className="side_post_image"><div><img src={process.env.PUBLIC_URL + "/assets/top_4.jpg"} alt /></div></div>
                                  <div className="side_post_content">
                                    <div className="side_post_title">How Did van Gogh’s Turbulent Mind</div>
                                    <small className="post_meta">Katy Liu<span>Sep 29</span></small>
                                  </div>
                                </div>
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* Advertising */}
                </div>
              </div>
            </div>
          </div>
        </div>

    <Footer/>    
      </div>

      
    );
  }
}

export default Category;
