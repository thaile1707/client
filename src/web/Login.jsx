import React, { Component } from "react";
import Footer from "../components/Footer";
import Navbar from "../components/Navbar";
import '../libs/styles/contact.css'
import '../libs/styles/contact_responsive.css'


class Signin extends Component {
  render() {
    return (
        <React.Fragment>
            <Navbar/>
           <div className="home">
            <div className="home_background parallax-window" data-parallax="scroll" data-image-src={process.env.PUBLIC_URL + "/assets/regular.jpg"} data-speed="0.8" />
            <div className="home_content">
                <div className="container">
                <div className="row">
                    <div className="col-lg-15 ">
                    {/* Post Comment */}
                    <div className="post_comment">
                        <div className="contact_form_container">
                        <form action="#">
                            <input type="text" className="contact_input contact_input_name" placeholder="Your Name" required="required" />
                            <input type="email" className="contact_input contact_input_email" placeholder="Your Email" required="required" />
                            <textarea className="contact_text" placeholder="Your Message" required="required" defaultValue={""} />
                            <button type="submit" className="contact_button">Send Message</button>
                        </form>
                        </div>
                    </div>
                    </div>
                </div>
                </div>		
            </div>
            </div>
            <Footer/>
        </React.Fragment>
           


        );
    }
}

export default Signin;
